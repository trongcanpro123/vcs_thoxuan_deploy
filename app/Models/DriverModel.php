<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class DriverModel extends BaseModel
{
    protected $table = 'manager_car_in_out';
    protected $primaryKey = 'id';
    
    protected $useSoftDeletes = false;
    protected $allowedFields = ['checkin_image_up', 'checkin_image_front', 'checkin_image_left', 'checkin_image_right', 'checkout_image_up', 'checkout_image_front', 'checkout_image_left', 'checkout_image_right', 'construction_id', 'construction_name', 'construction_address', 'receipt', 'delivery_unit', 'car_id', 'car_type', 'material_code', 'material_name', 'material_id', 'delivery_id', 'delivery_method', 'input_volume', 'reduction_volume', 'actual_volume', 'car_number', 'status', 'checkin_time', 'checkout_time', 'created_by', 'created_time', 'udpated_by', 'udpated_time', 'record_status', 'notification', 'unit'];
    
    
/**
     * @param int $limit
     * @param int $offset
     * @param bool $activeRecord
     * @return array|null
     */
    public function findAll(int $limit = 0, int $offset = 0, bool $activeRecord = true)
    {
        // $data = parent::findAll($limit, $offset);

        $builder = $this->builder();

		if ($this->tempUseSoftDeletes === true)
		{
			$builder->where($this->table . '.' . $this->deletedField, null);
		}

        $builder->orderBy($this->primaryKey, 'DESC');

		$row = $builder->limit($limit, $offset)
				->get();

		$row = $row->getResult($this->tempReturnType);

		$row = $this->trigger('afterFind', ['data' => $row, 'limit' => $limit, 'offset' => $offset]);

		$this->tempReturnType     = $this->returnType;
		$this->tempUseSoftDeletes = $this->useSoftDeletes;

		// $data = $row['data'];

        // echo '<pre>';print_r($data);die;

        if (!$activeRecord) return $row['data'];
        if (!$row['data'] || empty($row['data'])) return null;

        $rs=  array_map(function ($item) {
            return $this->_assign($item);
        }, $row['data']); 
        // echo '<pre>';print_r($rs);die;

        return $rs;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param bool $activeRecord
     * @return array|null
     */
    // public function findAll(int $limit = 0, int $offset = 0, bool $activeRecord = true)
    // {
    //     return parent::findAll($limit, $offset);
    //     // echo '<pre>';print_r($data);die;
    //     // if (!$activeRecord) return $data;

    //     // if (!$data || empty($data)) return null;

    //     // return array_map(function ($item) {
    //     //     return $this->_assign($item);
    //     // }, $data);
    // }

    /**
     * @param array $data
     * @return mixed
     */
    private function _assign(array $data)
    {
        
        $className = get_called_class();

        $model = new $className();

        $rules = $this->getRules();

        $rule = null;

        foreach ($data as $attribute => $value) {
            if ($attribute === $this->primaryKey || (isset($rules[$attribute]) &&
                    ($rule = $rules[$attribute]) !== null) || in_array($attribute, $this->allowedFields)) {
                $model->$attribute = $this->_formatByRule($rule, $value);
            }
        }
        // echo '<pre>';print_r($m  odel);die;
        return $model;
    }

    private function _formatByRule($rule, $value)
    {
//        if (strpos($rule, 'integer') > -1) {
//            echo $attribute; die;
//            return (int)$value;
//        }
        return $value;
    }

    /**
     * @param string|null $scenario
     * @return array
     */
    // public function export_excel_filter($car_numer)
    // {

    //     return $this->db->query('SELECT * FROM manager_car_in_out  WHERE pump_system_id = ? AND area_id = ? AND NOT id = ?', [$pump_system_id, $area_id, $this->getPrimaryKey()])->getResultArray();
    // }
    public function select_export_excel()
    {
        return $this->db->query('SELECT  `construction_name`, `construction_address`, `receipt`, `car_number`, `car_type`,`delivery_unit`, `material_name`, `material_code`, `input_volume`,`reduction_volume`,`actual_volume`,`checkin_time`  FROM manager_car_in_out')->getResultArray();
    }
    // public function get_list_throad_alert()
    // {
    //     return $this->db->query('SELECT  `notification` FROM manager_car_in_out')->getResultArray();
    // }
    public function get_list_throad_alert()
    {
        return json_encode($this->db->query('SELECT COUNT(*) Count_Noti FROM manager_car_in_out WHERE notification = 0')->getResultArray());
    }
    public function update_list_throad_alert()
    {
        return json_encode($this->db->query('update manager_car_in_out set notification = 1')->getResultArray());
    }
    public function getRules(string $scenario = null): array
    {
        return [
            'checkin_image_up' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh chính diện không được để trống',
                ]
            ],
            'checkin_image_front' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh trái không được để trống',
                ]
            ],
            'checkin_image_left' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh phải không được để trống',
                ]
            ],
            // 'construction_name' =>[
            //     'rules'  => 'required',
            //     'errors' => [
            //         'required' => 'Tên công trình không được để trống',
            //     ]
            // ],
            // 'car_number' =>[
            //     'rules'  => 'required',
            //     'errors' => [
            //         'required' => 'Biển số xe không được để trống',
            //     ]
            // ],
            // 'actual_volume' =>[
            //     'rules'  => 'required',
            //     'errors' => [
            //         'required' => 'Khối thực nhập phải lơn hơn 0',
            //     ]
            // ],

        ];
    }

    

}