<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Chi tiết vào ra';
?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>

            </div>
            <div class="card-body ">
                <form action="<?php base_url() . '/Administrator/in_out_detail'; ?>" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <select name="area_id" class="form-control mr-sm-2" placeholder="Chọn kho">
                                <?php
                                $current_user = (\App\Models\AdministratorModel::findIdentity());
                                if ($area_models) {
                                    foreach ($area_models as $area) {
                                        if($current_user->type_user == 'lanh_dao' || $current_user->area_id == $area->id){
                                            ?>
                                                <option value="<?= $area->id ?>" <?php if ($area->id == $param_search['area_id']) {
                                                    echo 'selected';
                                                } ?> ><?= $area->name ?></option>
                                            <?php
                                        }
                                    }
                                }

                                ?>
                            </select>

                        </div>
                        <div class="col-md-3">
                            <input name="car_number" class="form-control mr-sm-2" type="search" placeholder="Biển số"
                                   aria-label="Search" value="<?= $param_search['car_number'] ?>">
                        </div>
                        <div class="col-md-2">
                            <div class='input-group date'>
                                <input type='text' name="start_time" placeholder="Từ ngày"
                                       class="form-control datetimepicker" autocomplete="off"
                                       value="<?= $param_search['start_time'] ?>"/>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class='input-group date'>
                                <input type='text' name="end_time" placeholder="Đến ngày"
                                       value="<?= $param_search['end_time'] ?>" autocomplete="off"
                                       class="form-control datetimepicker"/>

                            </div>
                        </div>
                        <div class="col-md-2 text-center">
                            <button class="btn btn-info btn-round" type="submit">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card " style="margin-top: 0px">

            <div class="card-body table-responsive">


                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Biển số</th>
                        <th>Giờ vào</th>
                        <th class="text-center">Kiểm tra vào</th>
                        <th class="text-center">Cấp STT</th>
                        <th class="text-center">Lấy ticket</th>
                        <th class="text-center">Lấy hàng</th>
                        <th class="text-center">Kiểm tra ra</th>
                        <th class="text-center">Ra khỏi kho</th>
                        <th>Ghi chú</th>
                    </tr>
                    </thead>
                    <tbody id="myTable">
                    <?php if ($models): ?>
                        <?php foreach ($models as $model):
                            $button_check = ' <button class="btn btn-round btn-success btn-sm" value="X"> X </button>';
                            ?>
                            <tr>
                                <td><?= $model->car_number ?></td>
                                <td><?= date('d/m/Y H:i', strtotime($model->checkin_time)) ?></td>
                                <td class="text-center"><?php if ($model->checkin_time) {
                                        echo $button_check;
                                    } ?></td>
                                <td class="text-center"><?php if ($model->checkin_order > '0') {
                                        echo $button_check;
                                    } else {
                                        echo '';
                                    } ?></td>
                                <td class="text-center"><?php if ($model->ticket == '1') {
                                        echo $button_check;
                                    } else {
                                        echo '';
                                    } ?></td>
                                <td class="text-center"><?php if ($model->checkout_order_status == '1') {
                                        echo $button_check;
                                    } else {
                                        echo '';
                                    } ?></td>
                                <td class="text-center"><?php if ($model->checkout_time) {
                                        echo $button_check;
                                    } ?></td>
                                <td class="text-center"><?php if ($model->checkout_time) {
                                        echo $button_check;
                                    } ?></td>
                                <td><?= $model->checkin_note ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif ?>
                    </tbody>
                </table>
                <?= $pager->links('default', 'default_cms') ?>
            </div>
        </div>
    </div>
</div>


