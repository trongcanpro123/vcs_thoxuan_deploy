<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Quản lý người dùng';
?>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
        <a href="<?= route_to('administrator_create') ?>" class="btn btn-warning btn-round btn-sm">Thêm mới</a>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Họ và Tên</th>
                <th>Tài khoản</th>
                <th>Vai trò</th>
                <th>Kho</th>
                <th>Ghi chú</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!$models || empty($models)): ?>
                <tr>
                    <td colspan="100">
                        <div class="empty-block">
                            <img src="/images/no-content.jpg" alt="No content"/>
                            <h4>Không có nội dung</h4>
                            <a class="btn btn-info btn-round"
                               href="<?= route_to('administrator_create') ?>">Thêm</a>
                        </div>
                    </td>
                </tr>
            <?php else: ?>
                <?php foreach ($models as $model): ?>
                    <tr>
                        <td><?= $model->full_name ?></td>
                        <td><?= Html::decode($model->account_name) ?></td>
                        <td><?php if ($model->type_user == 'lanh_dao') {
                                echo 'Lãnh đạo';
                            } elseif ($model->type_user == 'lanh_dao_kho') {
                                echo 'Lãnh đạo kho';
                            } elseif ($model->type_user == 'nhan_vien') {
                                echo 'Nhân viên';
                            } else {
                                echo 'Bảo vệ';
                            }
                            ?></td>
                        <td><?= $model->select_name_area() ?></td>
                        <td><?= Html::decode($model->note) ?></td>
                        <td class="row-actions">
                            <a href="<?= route_to('administrator_update', $model->getPrimaryKey()) ?>"
                               class="btn btn-info btn-just-icon btn-sm" title="sửa">
                                <i class="material-icons">edit</i>
                            </a>
                            <a href="<?= route_to('administrator_delete', $model->getPrimaryKey()) ?>"
                               class="btn btn-danger btn-just-icon btn-sm" data-method="post" title="xóa"
                               data-prompt="Bạn có chắc muốn xoá tài khoản này?">
                                <i class="material-icons">delete</i>
                            </a>
                            <a href="<?= route_to('administrator_password', $model->getPrimaryKey()) ?>"
                               class="btn btn-warning btn-just-icon btn-sm" title="đổi mật khẩu">
                                <i class="material-icons ">vpn_key</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>
            </tbody>
        </table>
        <?= $pager->links('default', 'default_cms') ?>

    </div>
</div>
