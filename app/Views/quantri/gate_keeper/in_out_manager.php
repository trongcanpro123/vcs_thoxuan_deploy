<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Kiểm soát vào ra';
?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="margin-bottom: 0px">
                    <div class="card-header card-header-text card-header-info">
                    </div>
                    <div class="card-body" style="padding-top: 0px">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_in_test">
                                        
                                                <option value="http://127.0.0.1:2003/iptv/cam1.flv" ></option>

                                    </select>
                                </div>
                                <div class="">
                                    <a href="/quantri/driver/create" type="button" class="btn btn-primary " style="color: white">Quản lý ra vào</a>
                                    <a href="/quantri/car/create" type="button" class="btn btn-rose" style="color: white">Đăng ký xe</a>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_out">
                                        <?php if($camera_out) {
                                            foreach ($camera_out as $camera){
                                                ?>
                                                <option value="http://127.0.0.1:2003/iptv/cam4.flv" ></option>
                                                <?php
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_in">
                                        <?php if($camera_in) {
                                                ?>
                                                <option value="http://127.0.0.1:2003/iptv/cam3.flv" ></option>

                                                <?php
                                            
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_out1">
                                        <?php if($camera_out) {
                                            foreach ($camera_out as $camera){
                                                ?>
                                                <option value="http://127.0.0.1:2003/iptv/cam2.flv" ></option>
                                                <?php
                                            }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6 div_camera_in1312324 ">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100%" id="videoElement_in_test"></video>
                        <canvas style="display: none" id="canvas_videoElement_in" ></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 div_camera_out">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100% text-center" id="videoElement_out"></video>
                        <canvas style="display: none" id="canvas_videoElement_out" ></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 div_camera_in ">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100%" id="videoElement_in"></video>
                        <canvas style="display: none" id="canvas_videoElement_in" ></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 div_camera_out1">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body text-center">
                        <video width="100% text-center" id="videoElement_out1"></video>
                        <canvas style="display: none" id="canvas_videoElement_out1" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var is_on_change = false;
    window.onload = function() {

        change_play_cam_gate_in($('#select_camera_in_test'), 'videoElement_in_test');
        change_play_cam_gate_in1($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');
        change_play_cam_gate_out1($('#select_camera_out1'), 'videoElement_out1');
        // setInterval(refresh_cam, 60*1000);

        document.addEventListener("visibilitychange", visibilitychange);

    };

    function visibilitychange (evt) {
        if(is_on_change) return;
        console.log('refresh22222222222222222222222222');
        is_on_change = true;
        refresh_cam();
        is_on_change = false;
    }
</script>