<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = '';
?>
<!doctype html>
<html>
<head>

    <title><?= $this->title ?></title>
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <style>
        html {margin:0;padding:0;}
        /*@media print {*/
        /*    body {*/
        /*        width: 300px*/
        /*        height: 600px*/
        /*    }*/
        /*    !* etc *!*/
        /*}*/
        @media print {
            #printarea * {
                display:block;
            }
            .not_print{
                display: none;
                margin: 0px;
            }
        }
        body{
            width: 595px;
            height: 842px;
            font-family: Arial;
            margin: 0;
            padding: 0;
        }
        @page {
            margin: 30mm 20mm 30mm 15mm;
        }
    </style>
</head>
<body>
<button class="not_print" style="height: 30px" onclick="window.print();">In Phiếu</button>
<table width="100%">
    <tbody>
    <tr>
       <td colspan="1" style="text-align: center"> <image style="margin: 0; height: 30px" src="/images/logo-xd.png"  alt=""></td>
       <td colspan="1" style="text-align: center"> 
        <div style="font-weight: bold; font-size: 14px; margin-bottom: 5px; margin-top: 5px">Tên công trình: <?= $model->construction_name?></div></td>
    </tr>
    <tr>
        <td colspan="2" style="vertical-align:bottom; text-align: center">
            <div style="font-weight: bold; font-size: 16px; margin-bottom: 5px; margin-top: 3px">PHIẾU NHẬP KHO</div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="vertical-align:bottom; text-align: center">
            <div style="font-weight: bold; font-size: 12px; margin-bottom: 5px; margin-top: 3px">Số phiếu: <?= $model->receipt?></div>
        </td>
    </tr>
    <tr >
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Thời gian vào: <?= date('d-m-Y H:i:s', strtotime($model->checkin_time))?></div>
        </td>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Thời gian ra: <?= date('d-m-Y H:i:s', strtotime($model->checkout_time))?></div>
        </td>
    </tr>
    <tr >
        <td style="width: 30%">
            <div style="float: left;font-weight: bold; font-size: 12px;margin-bottom: 5px">Địa điểm giao nhận:</div>
        </td>
        <td>
            <div style="float: left;font-weight: bold; font-size: 12px;margin-bottom: 5px"><?= $model->construction_address?></div>
        </td>
    </tr>
    <tr >
        <td style="width: 30%">
            <div style="float: left;font-weight: bold; font-size: 12px;margin-bottom: 5px">Đơn vị giao hàng:</div>
        </td>
        <td>
            <div style="float: left;font-weight: bold; font-size: 12px;margin-bottom: 5px"><?= $model->delivery_unit?></div>
        </td>
    </tr>
    <tr >
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Loại xe: <?= $model->car_type?></div>
        </td>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Loại hàng hoá: <?= $model->material_name?></div>
        </td>
    </tr>
    <tr >
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Biển số xe: <?= $model->car_number?></div>
        </td>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Phương pháp giao nhận: <?= $model->delivery_method?></div>
        </td>
    </tr>
    <tr >
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Khối lượng nhập: <?= $model->input_volume?></div>
        </td>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Khối lượng giảm trừ: <?= $model->reduction_volume?></div>
        </td>
    </tr>
    <tr>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Ảnh cam trên</div>
        </td>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Ảnh cam bên phải</div>
        </td>
    </tr>
    <tr>
        <td colspan="1" style="text-align: left">  <img style="margin: 0; height: 100px" src="<?= $model->checkin_image_up ? $model->checkin_image_up : '/images/empty.jpg' ?>" />
        <td colspan="2" style="text-align: left">  <img style="margin: 0; height: 100px" src="<?= $model->checkin_image_front ? $model->checkin_image_front : '/images/empty.jpg' ?>" />
    </tr>
    <tr>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Ảnh cam bên trái</div>
        </td>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Ảnh cam giữa</div>
        </td>
    </tr>
    <tr>
        <td colspan="1" style="text-align: left">  <img style="margin: 0; height: 100px" src="<?= $model->checkin_image_left ? $model->checkin_image_left : '/images/empty.jpg' ?>" />
        <td colspan="2" style="text-align: left">  <img style="margin: 0; height: 100px" src="<?= $model->checkin_image_right ? $model->checkin_image_right : '/images/empty.jpg' ?>" />
    </tr>
    <tr>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Ảnh checkout ra trên</div>
        </td>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 10px">Ảnh check out ra bên phải </div>
        </td>
    </tr>
    <tr>
        <td colspan="1" style="text-align: left">  <img style="margin: 0; height: 100px" src="<?= $model->checkout_image_up ? $model->checkout_image_up : '/images/empty.jpg' ?>" />
        <td colspan="2" style="text-align: left">  <img style="margin: 0; height: 100px" src="<?= $model->checkout_image_front ? $model->checkout_image_front : '/images/empty.jpg' ?>" />
    </tr>
    <tr>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Thủ kho xác nhận</div>
        </td>
        <td style="width: 50%">
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Kế toán xác nhận</div>
        </td>
    </tr>
    <tr>
        <td style="width: 50%">
        <br><br><br>
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Cán bộ KT xác nhận</div>
        </td>
        <td style="width: 50%">
        <br><br><br>
        <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 5px">Người giao hàng xác nhận</div>
        </td>
    </tr>
    </tbody>

</body>
<script>
    window.onbeforeunload = function(){
        // set warning message
        window.opener.location.href="/quantri/danh-sach-tai-xe";
    };
</script>
</html>