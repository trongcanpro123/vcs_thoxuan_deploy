<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Quản lý vào ra';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="card-body ">
                <form action="<?= route_to('driver') ?>" method="GET">
                    <div class="row">
                        <div class="col-md-4">
                            <input placeholder="Biển số" type="text" name="car_number" autocomplete="off" class="form-control" autofocus=""
                                   value="<?= $param_search['car_number'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class='input-group date' >
                                <input   type='text' name="start_time" placeholder="Từ thời gian"
                                         class="form-control datetimepicker" autocomplete="off" value="<?= $param_search['start_time'] ?>"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class='input-group date' >
                                <input type='text' name="end_time" placeholder="Tới thời gian"
                                       value="<?=  $param_search['end_time'] ?>" autocomplete="off" class="form-control datetimepicker"/>
                            </div>
                    </div>
                    <div class="col-md-4">
                             <input type="submit" autocomplete="off" class="btn btn-info btn-round" value="Tìm kiếm">
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
        <a href="<?= route_to('admin_driver_create') ?>" class="btn btn-warning btn-round btn-sm">Thêm mới</a>
        <a href="<?= route_to('nv_export_excel') ?>" class="btn btn-warning btn-round btn-sm">Kết xuất báo cáo</a>
    </div>
    
    <div class="card-body table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="text-align: center">STT</th>
                    <th style="text-align: center">Công trình</th>
                    <th style="text-align: center">Biển số</th>
                    <th style="text-align: center">Loại xe</th>
                    <th style="text-align: center">Đơn vị giao hàng</th>
                    <th style="text-align: center">Loại vật liệu</th>
                    <th style="text-align: center">Khối lượng nhập</th>
                    <th style="text-align: center">Khối lượng giảm trừ</th>
                    <th style="text-align: center">Thực nhập</th>
                    <th style="text-align: center">Số phiếu</th>
                    <th style="text-align: center">Thời gian vào</th>
                    <th style="text-align: center">Thời gian ra</th>
                    <th style="text-align: center">Trạng thái</th>
                    <th style="text-align: center">Hành động</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!$models || empty($models)): ?>
                    <tr>
                        <td colspan="100">
                            <div class="empty-block">
                                <img src="/images/no-content.jpg" alt="No content"/>
                                <h4>Không có nội dung</h4>
                                <a class="btn btn-info btn-round"
                                   href="<?= route_to('admin_driver_create') ?>">Thêm</a>
                            </div>
                        </td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($models as $key => $model): ?>
                        <tr>
                            <td class="row-actions text-center"><?= ++$key ?></td>
                            <td align="center"><?=$model->construction_name ?></td>
                            <td align="center"><?= Html::decode($model->car_number) ?></td>
                            <td align="center"><?= $model->car_type ?></td>
                            <td align="center"><?= $model->delivery_unit ?></td>
                            <td align="center"><?= $model->material_name ?></td>
                            <!-- <td><?= $model->delivery_method ?></td> -->
                            <td align="center"><?= $model->input_volume ?></td>
                            <td align="center"><?= $model->reduction_volume ?></td>
                            <td align="center"><?= $model->actual_volume ?></td>
                            <td align="center"><?= $model->receipt ?></td>
                            <td align="center"><?= $model->created_time ?></td>
                            <td align="center"><?= $model->checkout_time ?></td>
                            <td align="center"><?= $model->status?></td>
                            <td class="row-actions">
                                <?= Html::a('<i class="material-icons">edit</i>', ['admin_driver_update', $model->getPrimaryKey()], [
                                    'class' => ['btn', 'btn-info', 'btn-just-icon','btn-sm'],
                                    'title' => 'Sửa'
                                ]) ?>
                                <!-- <a href="<?= route_to('admin_driver_delete', $model->getPrimaryKey()) ?>"
                                   class="btn btn-danger btn-just-icon btn-sm" data-method="post"
                                   data-prompt="Bạn có chắc sẽ xoá đi mục này?">
                                    <i class="material-icons">delete</i>
                                </a> -->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif ?>
                </tbody>
            </table>
            <?= $pager->links('default', 'default_cms') ?>
        </div>
 </div>
