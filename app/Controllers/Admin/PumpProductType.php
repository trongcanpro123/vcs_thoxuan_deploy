<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Helpers\ArrayHelper;
use App\Helpers\SessionHelper;
use App\Models\AdministratorModel;
use App\Models\AreaModel;
use App\Models\CarInOutHistoryModel;
use App\Models\PumpProductTypeModel;
use App\Models\PumpSystemModel;
use CodeIgniter\Model;
use DateTime;
class PumpProductType extends BaseController
{
    /**
     * @return string
     */
    public function index()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }
        $model = (new PumpProductTypeModel());

        return $this->render('pump_product_type/index', [
            'models' => $model->paginate(),
            'pager' => $model->pager
        ]);
    }

    public function create()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $model = new PumpProductTypeModel();

        if ($this->isPost() && $this->validate($model->getRules())) {
            try {
                $model->loadAndSave($this->request, function ($request, array $data) {
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Thêm mới thành công'
                ]);

                return $this->response->redirect(route_to('nv_pump_product_type'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }

        return $this->render('pump_product_type/create', [
            'model' => $model,
            'validator' => $this->validator
        ]);
    }

    public function update($id)
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }
        $model = (new PumpProductTypeModel())->find($id);

        if ($this->isPost() && $this->validate($model->getRules())) {
            try {
                $model->loadAndSave($this->request, function ($request, array $data) {
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Sửa thành công'
                ]);

                return $this->response->redirect(route_to('nv_pump_product_type'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }
        return $this->render('pump_product_type/update', [
            'model' => $model,
            'validator' => $this->validator
        ]);
    }

    public function delete($id){
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        /** @var PumpProductTypeModel $model */
        if (!$this->isPost() || !($model = (new PumpProductTypeModel())->find($id))) {
            return $this->renderError();
        }

        SessionHelper::getInstance()->setFlash('ALERT', [
            'type' => 'warning',
            'message' => 'Xoá thành công'
        ]);
        $model->delete($model->getPrimaryKey());
        return $this->response->redirect(route_to('nv_pump_product_type'));
    }

}